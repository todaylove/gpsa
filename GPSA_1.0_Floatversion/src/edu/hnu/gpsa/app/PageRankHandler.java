package edu.hnu.gpsa.app;

import edu.hnu.gpsa.core.Handler;

public class PageRankHandler implements Handler{

	@Override
	public float init(int sequence) {
		return 1.0f;
	}

	@Override
	public float compute(float val, float mVal) {
		return (float) (val + 0.85 * mVal);
	}

}
